from django.shortcuts import render
from django.contrib.auth import get_user_model
from rest_framework import (
    generics,
    # viewsets,
)
from .models import Post
from .serializers import (
    PostSerializer,
    UserSerializer,
)
from .permissions import IsAuthorOrReadOnly
# Create your views here.

# class PostViewSet(viewsets.ModelViewSet):
#     serializer_class = PostSerializer
#     queryset = Post.objects.all()
#     permission_classes = (IsAuthorOrReadOnly,)

# class UserViewSet(viewsets.ModelViewSet):
#     serializer_class = UserSerializer
#     queryset = get_user_model().objects.all()

class PostList(generics.ListCreateAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    permission_classes = (IsAuthorOrReadOnly,)

class UserList(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
