from django.test import TestCase
from .models import Post
from django.contrib.auth.models import User
# Create your tests here.

class BlogTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        # create testusers
        testuser1 = User.objects.create(
            username='testuser1', password='abc123')
        testuser1.save()

        # create a blog post
        testpost = Post.objects.create(
            author = testuser1,
            title = 'my test title',
            body = 'A test journey',
        )
        testpost.save()

    def test_blog_content(self):
        post = Post.objects.get(id=1)
        self.assertEqual(post.title, 'my test title')
        self.assertEqual(post.author.username, 'testuser1')
        self.assertEqual(post.body, 'A test journey')
